﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAppBlazor.Shared.Models
{
    public class CalcByPaymentModel
    {
        [JsonProperty("vrste_placanja_uid")]
        public string? PaymentTypeId { get; set; }
        [JsonProperty("naziv")]
        public string? Name { get; set; }
        [JsonProperty("iznos")]
        public decimal Amount { get; set; }
        [JsonProperty("nadgrupa_placanja_uid")]
        public string? ParentTypeId { get; set; }
        [JsonProperty("nadgrupa_placanja_naziv")]
        public string? ParentTypeName { get; set; }
    }
}
