﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoAppBlazor.Shared.Models
{
    public class CalcByProductModel
    {
        [JsonProperty("artikl_uid")]
        public string? ProductId { get; set; }
        [JsonProperty("naziv_artikla")]
        public string? ProductName { get; set; }
        [JsonProperty("kolicina")]
        public decimal Quantity { get; set; }
        [JsonProperty("iznos")]
        public decimal Amount { get; set; }
        [JsonProperty("usluga")]
        public char IsService { get; set; }

    }
}
