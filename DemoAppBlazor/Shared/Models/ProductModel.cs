﻿using Newtonsoft.Json;

namespace DemoAppBlazor.Shared.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        [JsonProperty("naziv")]
        public string? Name { get; set; }
    }
}
