﻿using System.ComponentModel.DataAnnotations;

namespace DemoAppBlazor.Client.Models
{
    public class CalcFormModel
    {
        [Required]
        public string? UnitId { get; set; }
        [Required]
        [Range(typeof(DateTime), minimum: "2000-01-01", maximum: "2030-05-14", ErrorMessage = "Value for {0} must be more than {1}")]
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        
    }
}
