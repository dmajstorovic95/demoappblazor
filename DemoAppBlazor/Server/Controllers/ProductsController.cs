﻿using DemoAppBlazor.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Text;

namespace DemoAppBlazor.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IConfiguration _config;

        public ProductsController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet("name")]
        public async Task<IList<ProductModel>> Get(string name = "")
        {
            IList<ProductModel> products = new List<ProductModel>();
            var creds = _config["apicreds:UserAndPass"];

            var client = new HttpClient();
            var header = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(creds)));
            client.DefaultRequestHeaders.Authorization = header;
            using var response = await client.GetAsync($"http://apidemo.luceed.hr/datasnap/rest/artikli/naziv/{name}");
            if (response.IsSuccessStatusCode)
            {
                string? jsonResult = await response.Content.ReadAsStringAsync();
                dynamic jsonObj = JsonConvert.DeserializeObject(jsonResult);
                JObject? innerObj = jsonObj["result"][0] as JObject;
                if (innerObj is not null)
                {
                    IList<JToken> tokens = innerObj["artikli"].Children().ToList();
                    foreach (var token in tokens)
                    {
                        ProductModel product = token.ToObject<ProductModel>();
                        products.Add(product);
                    }

                }
                return products;
            }
            else
            {
                throw new Exception(response.ReasonPhrase);
            }
        }

    }
}
