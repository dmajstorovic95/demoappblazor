﻿using DemoAppBlazor.Shared.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Text;

namespace DemoAppBlazor.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalcByProductController : ControllerBase
    {
        private readonly IConfiguration _config;

        public CalcByProductController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet("byproduct")]
        public async Task<IList<CalcByProductModel>> GetAll(string unitId, string dateFrom, string? dateTo = "")
        {
            IList<CalcByProductModel> calcs = new List<CalcByProductModel>();
            var creds = _config["apicreds:UserAndPass"];

            var client = new HttpClient();
            var header = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(creds)));
            client.DefaultRequestHeaders.Authorization = header;
            using var response = await client.GetAsync($"http://apidemo.luceed.hr/datasnap/rest/mpobracun/artikli/{unitId}/{dateFrom}/{dateTo}");
            if (response.IsSuccessStatusCode)
            {
                string? jsonString = await response.Content.ReadAsStringAsync();
                dynamic jsonObj = JsonConvert.DeserializeObject(jsonString);
                JObject? innerObj = jsonObj["result"][0] as JObject;
                if (innerObj is not null)
                {
                    IList<JToken> tokens = innerObj["obracun_artikli"].Children().ToList();
                    foreach (var token in tokens)
                    {
                        CalcByProductModel calc = token.ToObject<CalcByProductModel>();
                        calcs.Add(calc);
                    }
                }
                return calcs;
            }
            else
            {
                throw new Exception(response.ReasonPhrase);
            }
        }
    }
}
